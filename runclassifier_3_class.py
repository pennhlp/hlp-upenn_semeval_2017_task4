''''
*****-----*****
This system was submitted to the 2017 Semeval task. The system is built from the SimpleTextClassifier project codes.

The primary difference is that 2 features are introduced in addition to n-gram (sparse) vectors. They are:
1. Dense vectors using pre-trained Twitter Word Embeddings.
2. Generalized sparse vectors- vectors of word cluster numbers.

Identical to the SimpleTextClassifier, the runcrossvalidation_3_class.py is used to find optimal values for the cost
parameter and weights for the classes. runclassifier_3_class.py is used to perform the prediction on the test set.

*Requirements:*
- pandas, sklearn, nltk, numpy

*How to run:*
I had placed some hard-coded values for the weights in this file so that it can be quickly run... For best results,
Please use runcrossvalidation_3_class.py on your data and use the identified optimal values in this code.

The directory './english_training/' contains the Semeval annotated files.
They are available here (at competition time): http://alt.qcri.org/semeval2017/task4/index.php?id=data-and-tools

For dense word embeddings, pretrained vectors are used. We used the embeddings available here (at competition time):
http://www.fredericgodin.com/software/

The binary model and the extracted package contents should be placed in './word2vec_twitter_model/'
**please change the file path in the variable model_path in the code below to point to the right folder**

Cluster features are generated using the CMU Twitter clusters available here (at competition time):
http://www.cs.cmu.edu/~ark/TweetNLP/clusters/50mpaths2
**please place this file in the project folder**

Happy running. Citation details to be added soon...

Note: I will not be actively maintaining this code. Please email me at: abeed@upenn.edu for questions.

'''

from __future__ import division
import pandas as pd
from string import strip
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, cross_validation
from nltk.stem.porter import *
from nltk.corpus import stopwords
import numpy as np
import sys
import os
st = stopwords.words('english')
stemmer = PorterStemmer()
import codecs
import string
from word2vec_twitter_model.word2vecReader import Word2Vec
from supporting_functions import supporting_functions
from featureextractionmodules.FeatureExtractionUtilities import FeatureExtractionUtilities

import warnings
warnings.filterwarnings("ignore")


def loadDataAsDataFrame(f_path):
    '''
        Given a path, loads a data set and puts it into a dataframe
        - simplified mechanism
    '''
    datalist = []
    count = 0
    infile = codecs.open(f_path, 'r', encoding='utf-8')
    for line in infile:
        items = line.split('\t')
        if len(items) > 2:
            instance_dict = {}

            instance_dict['id'] = items[0]
            #instance_dict['class'] = items[1]
            try:
                instance_dict['text'] = strip(items[2].decode('iso-8859-1').encode('utf8'))
            except UnicodeEncodeError:
                ptext = items[2]
                instance_dict['text'] = strip(ptext)
            datalist.append(instance_dict)
            count += 1
    return pd.DataFrame(datalist)

def loadTrainingDataAsDataFrame():
    '''
        Given a path, loads a data set and puts it into a dataframe
        - simplified mechanism
    '''
    count = 0
    datalist = []
    f_path = './english_training/'
    dirlist = os.listdir('./english_training/')
    id_list = []
    for f in dirlist:
        print f
        #infile = open('./english_training/' + f)
        infile = codecs.open(f_path+f, 'r', encoding='utf-8')

        for line in infile:
            items = string.strip(line).split('\t')
            # print items
            if len(items) > 1:
                instance_dict = {}

                instance_dict['id'] = items[0]
                instance_dict['class'] = items[1]
                try:
                    instance_dict['text'] = strip(items[2].decode('iso-8859-1').encode('utf8'))
                except UnicodeEncodeError:
                    ptext = items[2]
                    instance_dict['text'] = strip(ptext)
                #count += 1
                if instance_dict['id'] not in id_list:
                    datalist.append(instance_dict)
                    id_list.append(instance_dict['id'])
                    count += 1
        infile.close()
    print count
    return pd.DataFrame(datalist)

def loadFeatureExtractionModuleItems():
    FeatureExtractionUtilities.loadItems()


def stem_and_tokenize(raw_text):
    '''
        Function that simply stems the terms using the porter stemmer
        and tokenizes them.
    '''
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    return (" ".join(words))


if __name__ == '__main__':
    '''
    paramsinfile = open('best_params.txt')
    train_f_path =''
    test_f_path = ''
    if len(sys.argv)>2:
        train_f_path = sys.argv[1]
        test_f_path = sys.argv[2]
    else:
        f_path = 'labeledTrainData'
    '''

    cost =  16#float(strip(paramsinfile.readline().split()[1]))
    weights = {}
    loadFeatureExtractionModuleItems()
    model_path = '/Users/abeed/word2vec_twitter_model.bin'
    # model_path = "C:\\Users\\abeed\\Documents\\word2vec_twitter_model.bin"
    print("Loading the model, this can take some time...")
    embedding_model = Word2Vec.load_word2vec_format(model_path, binary=True)

    '''
    for line in paramsinfile:
        items = line.split('\t')
        class_ = items[0]
        weight = strip(items[1])
        weights[class_]=weight
    '''

    # load the dataset
    training_data = loadTrainingDataAsDataFrame()#loadDataAsDataFrame(train_f_path)
    #training_data = training_data.sample(frac=0.005)
    # obtain the texts
    training_data_texts = training_data['text']

    # prepare the vectorizer that generates one-hot vectors. trigrams are generated, which is generally good for text classification.
    # change the max_features number to vary performance..
    vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer="word", tokenizer=None, preprocessor=None,
                                 max_features=5000)
    clustervectorizer = CountVectorizer(ngram_range=(1, 1), analyzer="word", tokenizer=None, preprocessor=None,
                                        max_features=1000)

    # process the training data using the predefined function..
    tokens = []
    processed_training_data = []
    train_data_clusters =[]
    for t in training_data_texts:
        toks = stem_and_tokenize(t)
        train_data_clusters.append(FeatureExtractionUtilities.getclusterfeatures(t))

        processed_training_data.append(toks)
    trained_data_vectors = vectorizer.fit_transform(processed_training_data).toarray()
    train_data_cluster_vector = clustervectorizer.fit_transform(train_data_clusters).toarray()

    # get the embedding (dense) vectors
    training_tweet_embeddings = supporting_functions.get_embedding_vectors(
        supporting_functions.lowercase_and_remove_stopwords(training_data['text']), embedding_model)

    train_combined_vectors = np.concatenate((trained_data_vectors, training_tweet_embeddings), axis=1)
    train_combined_vectors = np.concatenate((train_combined_vectors, train_data_cluster_vector), axis=1)

    test_f_path = 'SemEval2017-task4-test.subtask-A.english.txt'
    test_data = loadDataAsDataFrame(test_f_path)
    test_data_texts = test_data['text']
    tokens = []
    test_data_clusters = []
    processed_test_data = []
    for t in test_data_texts:
        toks = stem_and_tokenize(t)
        test_data_clusters.append(FeatureExtractionUtilities.getclusterfeatures(t))
        processed_test_data.append(toks)
    test_data_vectors = vectorizer.transform(processed_test_data).toarray()
    test_data_cluster_vector = clustervectorizer.transform(test_data_clusters).toarray()

    test_tweet_embeddings = supporting_functions.get_embedding_vectors(
        supporting_functions.lowercase_and_remove_stopwords(test_data['text']),embedding_model)

    test_combined_vectors = np.concatenate((test_data_vectors, test_tweet_embeddings), axis=1)
    test_combined_vectors = np.concatenate((test_combined_vectors, test_data_cluster_vector), axis=1)

    #sorted_weights = sorted(class_weights.values())
    smaller_class_weight = 5.92#sorted_weights[0]
    middle_class_weight = 2.84#sorted_weights[1]
    bigger_class_weight = 2.66#sorted_weights[2]

    print 'smaller class weight'
    print smaller_class_weight
    print 'middle class weight'
    print middle_class_weight
    print 'bigger class weight'
    print bigger_class_weight

    '''
    # get the class name with the smaller and bigger weight
    smaller_class = [k for k, v in class_weights.items() if v == smaller_class_weight][0]
    middle_class = [k for k, v in class_weights.items() if v == middle_class_weight][0]
    bigger_class = [k for k, v in class_weights.items() if v == bigger_class_weight][0]
    '''
    #best_c = -1
    svm_classifier = svm.SVC(C=cost, cache_size=200, class_weight={'negative': smaller_class_weight, 'positive': bigger_class_weight, 'neutral': middle_class_weight},
                             coef0=0.0, degree=3, gamma='auto', kernel='rbf', max_iter=-1, probability=True,
                             random_state=None, shrinking=True, tol=0.001, verbose=False)
    svm_classifier = svm_classifier.fit( train_combined_vectors, training_data["class"] )

    result = svm_classifier.predict(test_combined_vectors)

    outfile = open('classificationresults.txt','w')

    for id_,class_,text in zip(test_data['id'],result,test_data['text']):
        line = id_ + '\t' + str(class_) + '\t' + text
        line = line.encode('utf8')
        try:
            outfile.write(line+'\n')
        except UnicodeEncodeError:
            print '** There was an error writing the result for '+ id_+' which has classification: ' + str(class_)
    print 'Results written to file: classificationresults.txt'
    outfile.close()
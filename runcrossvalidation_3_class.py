'''
*****-----*****
This system was submitted to the 2017 Semeval task. The system is built from the SimpleTextClassifier project codes.

The primary difference is that 2 features are introduced in addition to n-gram (sparse) vectors. They are:
1. Dense vectors using pre-trained Twitter Word Embeddings.
2. Generalized sparse vectors- vectors of word cluster numbers.

Identical to the SimpleTextClassifier, the runcrossvalidation_3_class.py is used to find optimal values for the cost
parameter and weights for the classes. runclassifier_3_class.py is used to perform the prediction on the test set.

*Requirements:*
- pandas, sklearn, nltk, numpy

*How to run:*

The directory './english_training/' contains the Semeval annotated files.
They are available here (at competition time): http://alt.qcri.org/semeval2017/task4/index.php?id=data-and-tools

For dense word embeddings, pretrained vectors are used. We used the embeddings available here (at competition time):
http://www.fredericgodin.com/software/

The binary model and the extracted package contents should be placed in './word2vec_twitter_model/'
**please change the file path in the variable model_path in the code below to point to the right folder**

Cluster features are generated using the CMU Twitter clusters available here (at competition time):
http://www.cs.cmu.edu/~ark/TweetNLP/clusters/50mpaths2
**please place this file in the project folder**

Happy running. Citation details to be added soon...

ote: I will not be actively maintaining this code. Please email me at: abeed@upenn.edu for questions.


*****-----*****

@author: asarker

'''

from __future__ import division
import pandas as pd
from string import strip
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, cross_validation
from nltk.stem.porter import *
from nltk.corpus import stopwords
import numpy as np
import os
import string
st = stopwords.words('english')
stemmer = PorterStemmer()
import codecs
from word2vec_twitter_model.word2vecReader import Word2Vec
from supporting_functions import supporting_functions
from featureextractionmodules.FeatureExtractionUtilities import FeatureExtractionUtilities

import warnings
warnings.filterwarnings("ignore")

def loadDataAsDataFrame():
    '''
        Given a path, loads a data set and puts it into a dataframe
        - simplified mechanism
    '''
    count = 0
    datalist = []
    f_path = './english_training/'
    dirlist = os.listdir('./english_training/')
    id_list = []
    for f in dirlist:
        print f
        #infile = open('./english_training/' + f)
        infile = codecs.open(f_path+f, 'r', encoding='utf-8')

        for line in infile:
            items = string.strip(line).split('\t')
            # print items
            if len(items) > 1:
                instance_dict = {}

                instance_dict['id'] = items[0]
                instance_dict['class'] = items[1]
                try:
                    instance_dict['text'] = strip(items[2].decode('iso-8859-1').encode('utf8'))
                except UnicodeEncodeError:
                    ptext = items[2]
                    instance_dict['text'] = strip(ptext)
                #count += 1
                if instance_dict['id'] not in id_list:
                    datalist.append(instance_dict)
                    id_list.append(instance_dict['id'])
                    count += 1
        infile.close()
    print count
    return pd.DataFrame(datalist)

def loadFeatureExtractionModuleItems():
    FeatureExtractionUtilities.loadItems()

def stem_and_tokenize(raw_text):
    '''
        Function that simply stems the terms using the porter stemmer
        and tokenizes them.
    '''
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    return (" ".join(words))

import random

if __name__ == '__main__':
    loadFeatureExtractionModuleItems()

    model_path = '/Users/abeed/word2vec_twitter_model.bin'

    #model_path = "C:\\Users\\abeed\\Documents\\word2vec_twitter_model.bin"
    print("Loading the model, this can take some time...")
    embedding_model = Word2Vec.load_word2vec_format(model_path, binary=True)
    random.seed(12314)
    # load the dataset
    training_data = loadDataAsDataFrame()
    #training_data = training_data[:50]
    # obtain the texts
    training_data = training_data.sample(frac=0.05)
    training_data_texts = training_data['text']
    train_data_clusters = []

    # prepare the vectorizer that generates one-hot vectors. trigrams are generated, which is generally good for text classification.
    # change the max_features number to vary performance..
    vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer="word", tokenizer=None, preprocessor=None, max_features=5000)
    clustervectorizer = CountVectorizer(ngram_range=(1, 1), analyzer="word", tokenizer=None, preprocessor=None,
                                        max_features=1000)

    # process the training data using the predefined function..
    tokens = []
    processed_training_data = []
    for t in training_data_texts:
        toks = stem_and_tokenize(t)
        train_data_clusters.append(FeatureExtractionUtilities.getclusterfeatures(t))

        processed_training_data.append(toks)
    trained_data_vectors = vectorizer.fit_transform(processed_training_data).toarray()
    train_data_cluster_vector = clustervectorizer.fit_transform(train_data_clusters).toarray()

    # get the embedding (dense) vectors
    training_tweet_embeddings = supporting_functions.get_embedding_vectors(
        supporting_functions.lowercase_and_remove_stopwords(training_data['text']), embedding_model)

    train_combined_vectors = np.concatenate((trained_data_vectors, training_tweet_embeddings), axis=1)
    train_combined_vectors = np.concatenate((train_combined_vectors, train_data_cluster_vector), axis=1)


    score_dict = {}
    # the program iterates through
    #keeping it simple; could use grid search from sklearn
    cost_vec = [1, 2, 4, 8, 16, 32, 64, 128, 256]
    # specify the number of folds
    folds = 10
    class_list = list(training_data['class'])
    class_names = list(set(class_list))
    class_name_counts = {}
    class_weights = {}

    #count the number of times each class occurs
    for c in class_names:
        class_name_counts[c] = class_list.count(c)
    #print the count for each class
    for k in class_name_counts.keys():
        print k, '\t', class_name_counts[k]
    #the weight for a class will be = #totalinstances/#instances of this class
    for k in class_name_counts.keys():
        class_weights[k] = sum(class_name_counts.values()) / class_name_counts[k]

    stdev = np.std(class_weights.values())
    interval = stdev * stdev
    print 'interval ' + str(interval)
    weights = []

    sorted_weights = sorted(class_weights.values())
    smaller_class_weight = sorted_weights[2]
    middle_class_weight = sorted_weights[1]
    bigger_class_weight = sorted_weights[0]

    print 'smaller class weight'
    print smaller_class_weight
    print 'middle class weight'
    print middle_class_weight
    print 'bigger class weight'
    print bigger_class_weight

    # get the class name with the smaller and bigger weight
    smaller_class = [k for k, v in class_weights.items() if v == smaller_class_weight][0]
    middle_class = [k for k,v in class_weights.items() if v == middle_class_weight][0]
    bigger_class = [k for k, v in class_weights.items() if v == bigger_class_weight][0]

    #simple weight tuning: the smaller class and the bigger class will be weighted by different amounts
    weight_range_smaller_class = np.arange(max(0.1,smaller_class_weight - (2 * interval)), smaller_class_weight + (2 * interval), interval)
    weight_range_bigger_class = np.arange(max(0.1,bigger_class_weight - (2 * interval)), bigger_class_weight + (2 * interval), interval)

    print 'interval'
    print interval

    print 'weight range '
    print weight_range_smaller_class

    print 'computing optimal parameters... THIS MAY TAKE A LONG TIME..'

    best_accuracy = 0.0
    best_c = -1
    best_weight = -1

    for c in cost_vec:
        for w1 in weight_range_smaller_class:
            for w2 in weight_range_bigger_class:
                #print 'Running config for.. cost = ' + str(c) + ' weight = ' + str(w) + '...'
                svm_classifier = svm.SVC(C=c, cache_size=200,
                                         class_weight={smaller_class: w1, bigger_class: w2, middle_class: middle_class_weight},
                                         coef0=0.0, degree=3,
                                         gamma='auto', kernel='rbf', max_iter=-1, probability=True, random_state=None,
                                         shrinking=True, tol=0.001, verbose=False)
                scores = cross_validation.cross_val_score(svm_classifier, train_combined_vectors, class_list,
                                                          n_jobs=-1, cv=folds, scoring='recall_macro')
                if scores.mean() > best_accuracy:
                    best_accuracy = scores.mean()
                    best_c = c
                    best_weight = w1
    print 'best accuracy : ' + str(best_accuracy)
    print 'best value for c: ' + str(best_c)
    print 'weight for smaller class: ' + str(best_weight) + ' and best weight for larger class: ' + str(bigger_class_weight)

    #write the values to a file so that runclassifier_2_class.py can use the file to classify test data
    outfile = open('best_params.txt','w')
    outfile.write('cost\t'+str(c)+'\n')
    outfile.write(smaller_class +'\t' + str(w1) + '\n')
    outfile.write(bigger_class+'\t'+str(bigger_class_weight)+'\n')
    outfile.close()